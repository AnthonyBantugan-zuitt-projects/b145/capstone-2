const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const auth = require("../auth");

// @desc	Add order
// @access	Public
router.post("/:userId/checkout", auth.verify, (req, res) => {
	const data = {
		userId: req.params.userId,
		payload: auth.decode(req.headers.authorization),
		reqBody: req.body,
	};
	orderController
		.addOrder(data)
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Get user order/s
// @access	User only
router.get("/:userId", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		params: req.params.userId,
	};
	orderController
		.getOrders(data)
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Get all orders
// @access	Admin only
router.get("/", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body,
	};
	orderController
		.getAllOrders(data)
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Update order status
// @access	Admin only
router.put("/:orderId/status", auth.verify, (req, res) => {
	const data = {
		orderId: req.params.orderId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body,
	};
	orderController
		.updateStatus(data)
		.then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
