const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

// @desc	Add product
// @access	Admin only
router.post("/create", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		payload: auth.decode(req.headers.authorization),
		reqBody: req.body,
	};
	productController
		.addProduct(data)
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Retrieve all active products
// @access	Public
router.get("/", (req, res) => {
	productController
		.activeProducts()
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Retrieve specific product
// @access	Public
router.get("/:productId", (req, res) => {
	productController
		.getProduct(req.params)
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Update product details
// @access	Admin only
router.put("/:productId/update", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization),
		reqBody: req.body,
	};
	productController
		.updateProduct(data)
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Set product to inactive (archive)
// @access	Admin only
router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	};
	productController
		.archiveProduct(data)
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Add to cart
// @access	User Only
router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		reqBody: req.body,
	};
	productController
		.addToCart(data)
		.then((resultFromController) => res.send(resultFromController));
});
module.exports = router;
