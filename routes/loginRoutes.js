const express = require("express");
const router = express.Router();
const loginController = require("../controllers/loginControllers");

// @desc	Register new user
// @access	Public
router.post("/register", (req, res) => {
	loginController
		.registerUser(req.body)
		.then((resultFromController) =>
			res.status(201).send(resultFromController)
		);
});

// @desc	Authenticate user
// @access	Public
router.post("/", (req, res) => {
	loginController
		.loginUser(req.body)
		.then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
