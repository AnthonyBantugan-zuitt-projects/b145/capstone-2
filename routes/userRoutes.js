const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

// @desc	Set user to admin
// @access	Admin Only
router.put("/:userId", (req, res) => {
	const data = {
		userId: req.params.userId,
		payload: auth.decode(req.headers.authorization),
		reqBody: req.body,
	};
	userController
		.makeUserAdmin(data)
		.then((resultFromController) =>
			res.status(201).send(resultFromController)
		);
});

// @desc	Retrieve all users
// @access	Admin only
router.get("/all", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	userController
		.getUsers(data)
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Retrieve specific user
// @access	Public
router.get("/:userId", (req, res) => {
	userController
		.getUser(req.params)
		.then((resultFromController) => res.send(resultFromController));
});

// @desc	Update user details
// @access	User only
router.put("/:userId/update", auth.verify, (req, res) => {
	const data = {
		userId: req.params.userId,
		payload: auth.decode(req.headers.authorization),
		reqBody: req.body,
	};
	userController
		.updateUser(data)
		.then((resultFromController) =>
			res.status(201).send(resultFromController)
		);
});

module.exports = router;
