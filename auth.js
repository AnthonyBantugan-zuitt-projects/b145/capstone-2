const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
const secret = process.env.JWT_SECRET;

dotenv.config();

// TOKEN CREATION
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		username: user.username,
		email: user.email,
		isAdmin: user.isAdmin,
	};
	return jwt.sign(data, secret, {});
};

// TOKEN VERIFICATION
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if (token !== undefined) {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({ authentication: "failed" });
			} else {
				next();
			}
		});
	} else {
		return res.send({ authentication: "failed" });
	}
};

// TOKEN DECRYPTION
module.exports.decode = (token) => {
	if (token !== undefined) {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null;
			} else {
				return jwt.decode(token, { complete: true }).payload;
			}
		});
	} else {
		return null;
	}
};
