const Product = require("../models/Product");
const router = require("../routes/productRoutes");
const auth = require("../auth");
const Order = require("../models/Order");
const User = require("../models/User");

// @desc	Add product
// @route	POST /api/products/create
// @access	Admin only
const addProduct = async (data) => {
	if (data.isAdmin === false)
		return `${data.payload.username} is not authorized`;

	const product = await Product.find({
		name: data.reqBody.name,
	});

	if (product.length > 0) return `Product ${product[0].name} already exist!`;

	let newProduct = new Product({
		name: data.reqBody.name,
		description: data.reqBody.description,
		quantity: data.reqBody.quantity,
		price: data.reqBody.price,
	});

	return newProduct.save().then((product, err) => {
		if (!err) return product;
	});
};

// @desc	Retrieve all active products
// @route	GET /api/products/
// @access	Public
const activeProducts = async () => {
	const activeProducts = await Product.find({ isActive: true });
	return activeProducts;
};

// @desc	Retrieve specific product
// @route	GET /api/products/:productId
// @access	Public
const getProduct = async (reqParams) => {
	const product = await Product.findById(reqParams.productId);
	return product;
};

// @desc	Update product details
// @route	PUT /api/products/:productId/update
// @access	Admin only
const updateProduct = async (data) => {
	if (data.payload.isAdmin === false)
		return `${data.payload.username} is not authorized!`;

	const product = await Product.findById(data.productId);

	product.name = data.reqBody.name;
	product.description = data.reqBody.description;
	product.price = data.reqBody.price;

	return product.save().then((updatedProduct, err) => {
		if (!err) return updatedProduct;
	});
};

// @desc	Set product to inactive (archive)
// @route	PUT /api/products/:productId/archive
// @access	Admin only
const archiveProduct = async (data) => {
	if (data.isAdmin === false) return `Not authorized!`;

	const product = await Product.findById(data.productId);

	product.isActive = false;

	return product.save().then((archivedProduct, err) => {
		if (!err) return `${archivedProduct.name} is now inactive`;
	});
};

// @desc	Add to cart
// @route	PUT /api/products/:productId
// @access	User Only
const addToCart = async (data) => {
	const user = await User.findById(data.userId);
	const product = await Product.findById(data.productId);
	const total = product.price * data.reqBody.quantity;

	if (data.isAdmin === true) return `Cannot order`;

	user.cart.push({
		productId: data.productId,
		name: product.name,
		price: product.price,
		quantity: data.reqBody.quantity,
		subtotal: total,
	});

	return user.save().then((userWithCart, err) => {
		if (!err) return userWithCart.cart;
	});
};

module.exports = {
	addProduct,
	activeProducts,
	getProduct,
	updateProduct,
	archiveProduct,
	addToCart,
};
