const router = require("../routes/userRoutes");
const User = require("../models/User");
const bcrypt = require("bcrypt");

// @desc	Set user to admin
// @route	PUT /api/user/:userId
// @access	Admin Only
const makeUserAdmin = async (data) => {
	// find a user
	const userData = await User.findById(data.userId);
	// Checks if logged user is admin
	if (data.payload.isAdmin === false)
		return `${data.payload.username} is not authorized`;
	// Checks if found user data is already admin
	if (userData.isAdmin === true)
		return `${userData.username} is already Admin`;
	// change user to admin
	userData.isAdmin = data.reqBody.isAdmin;
	// save new data to mongoDB
	return userData.save().then((nowAdmin, err) => {
		if (!err) return `${nowAdmin.username} is now Admin`;
	});
};

// @desc	Retrieve all users
// @route	GET /api/user/all
// @access	Admin only
const getUsers = async (data) => {
	// checks if user is an admin
	if (data.isAdmin === false) return `${data.username} is not authorized`;
	// find all users
	const users = await User.find({});
	// loop every 'users' in the array to not display the password in the output
	const noPasswordData = users.map((userData) => {
		// deconstruct to separate 'password' key from the object
		// '_.doc' is where mongoDb stores the data.
		const { password, ...otherDetails } = userData._doc;
		return otherDetails;
	});

	return noPasswordData;
};

// @desc	Retrieve specific user
// @route	GET /api/user/:userId
// @access	Public
const getUser = async (reqParams) => {
	const user = await User.findById(reqParams.userId);
	// deconstruct to separate 'password' key from the object
	// '_.doc' is where mongoDb stores the data.
	// const { password, ...otherDetails } = user._doc;
	const { username, email, mobile } = user._doc;

	return `${username}
	${email}
	${mobile}`;
};

// @desc	Update user details
// @route	PUT /api/user/:userId/update
// @access	User only
const updateUser = async (data) => {
	if (data.payload.id !== data.userId)
		return `${data.payload.username} is not authorized!`;

	const user = await User.findById(data.userId);

	user.username = data.reqBody.username;
	user.email = data.reqBody.email;
	user.password = bcrypt.hashSync(data.reqBody.password, 10);

	return user.save().then((updatedUser, err) => {
		if (!err) return `${updatedUser.username} updated sucessfully!`;
	});
};

module.exports = {
	makeUserAdmin,
	getUsers,
	getUser,
	updateUser,
};
