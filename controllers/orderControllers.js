const Order = require("../models/Order");
const router = require("../routes/orderRoutes");
const auth = require("../auth");
const req = require("express/lib/request");
const User = require("../models/User");

// @desc	Add order
// @route	POST /api/orders/:userId/checkout
// @access	User only
const addOrder = async (data) => {
	// Checks if token ID and params match
	if (data.userId != data.payload.id) return `Error`;

	const user = await User.findById(data.userId);
	// Maps all subtotal value in user cart
	const cartSubtotal = user.cart.map((subs) => subs.subtotal);
	// Add all subtotal value to get total amount
	const total = cartSubtotal.reduce((a, b) => a + b, 0);
	// Copy all cart values and store to another Array
	let userCart = user.cart.map((cart) => cart);

	let newOrder = new Order({
		userId: data.userId,
		firstname: data.reqBody.firstname,
		lastname: data.reqBody.lastname,
		address: data.reqBody.address,
		total: total,
	});

	// Add user cart data to order.products[]
	newOrder.products.push(userCart);

	return newOrder.save().then((order, err) => {
		if (!err) {
			user.orders.push({ orderId: order.id });
			user.cart.splice(0, user.cart.length);
			user.save();
		}
		return order;
	});
};

// @desc	Get user order/s
// @route	GET /api/orders/:userId
// @access	User only
const getOrders = async (data) => {
	// If user id and params not the same
	if (data.userId != data.params) return `Access denied!`;
	// Find orders with user id
	const orders = await Order.find({ userId: data.userId });
	// If no orders found
	if (orders.length === 0) return `No orders found!`;
	// output orders
	return orders;
};

// @desc	Get all orders
// @route	GET /api/orders/
// @access	Admin only
const getAllOrders = async (data) => {
	// If not admin: cannot get orders
	if (data.isAdmin === false) return `Admin access only!`;
	// Retrieve all orders
	// 3 options for status [Pending, Out for Delivery, Delivered]
	const orders = await Order.find({});
	// output
	return orders;
};

// @desc	Update order status
// @route	PUT /api/orders/:orderId/status
// @access	Admin only
const updateStatus = async (data) => {
	// Admin access only
	if (data.isAdmin === false) return `Admin access only`;

	const order = await Order.findById(data.orderId);

	order.status = data.reqBody.status;
	return order.save().then((updatedOrder, err) => {
		if (!err) return updatedOrder;
	});
};

module.exports = {
	addOrder,
	getOrders,
	getAllOrders,
	updateStatus,
};
