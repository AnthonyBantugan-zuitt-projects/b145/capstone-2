const router = require("../routes/loginRoutes");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// @desc	Register new user
// @route	POST /api/login/register
// @access	Public
const registerUser = async (reqBody) => {
	if (!reqBody.username || !reqBody.email || !reqBody.password)
		return `Please fill all fields`;

	const existingEmail = await User.find({ email: reqBody.email });
	const newUser = new User({
		username: reqBody.username,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobile: reqBody.mobile,
	});
	// checks if email already exist
	if (existingEmail.length > 0) return `User already exist!`;

	return newUser.save().then((user, err) => {
		if (!err) return `${user.username} registered successfully!`;
	});
};

// @desc	Authenticate user
// @route	POST /api/login/
// @access	Public
const loginUser = async (reqBody) => {
	// find user data
	const user = await User.findOne({ username: reqBody.username });
	// checks if no username is found in user data
	if (!user) return `Username does not exist!`;
	// compare user data password to request body password
	const password = bcrypt.compareSync(reqBody.password, user.password);
	// checks if "password" is false
	if (password !== true) return `Incorrect Password!`;
	// output user token
	return { token: auth.createAccessToken(user) };
};

module.exports = { registerUser, loginUser };
