// [SECTION] Dependecies
const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");

// [SECTION] Environment Variable Setup
const dotenv = require("dotenv");
dotenv.config();

// [SECTION] Modules
const loginRoutes = require("./routes/loginRoutes");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");

// [SECTION] Server Setup
const port = process.env.PORT;
const app = express();

// [SECTION] Database Connection
mongoose
	.connect(process.env.MONGO_URL, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	})
	.then(() => {
		console.log(`Connected to MongoDB`);
	})
	.catch((err) => {
		console.log(err);
	});

// [SECTION] Middlewares
app.use(express.json());
app.use(
	express.urlencoded({
		extended: true,
	})
);
app.use(cors());

// [SECTION] Server Routes
app.use("/api/login", loginRoutes);
app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes);

// [SECTION] Server Response
app.listen(port, () => {
	console.log(`Server running on port ${port}`);
});
