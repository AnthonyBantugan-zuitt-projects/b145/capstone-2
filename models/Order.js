const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema(
	{
		userId: {
			type: String,
			required: true,
		},
		firstname: {
			type: String,
			required: true,
		},
		lastname: {
			type: String,
			required: true,
		},
		address: {
			type: String,
			required: true,
		},
		products: Array,
		total: {
			type: Number,
			required: true,
			default: 0,
		},
		status: {
			type: String,
			default: "Pending",
		},
	},
	{ timestamps: true }
);

module.exports = mongoose.model("Order", orderSchema);
