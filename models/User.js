const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
	{
		username: {
			type: String,
			required: [true, `Username is required`],
			unique: true,
		},
		email: {
			type: String,
			required: [true, `Email is required`],
			unique: true,
		},
		password: {
			type: String,
			required: [true, `Password is required`],
		},
		mobile: {
			type: String,
			required: true,
		},
		cart: [
			{
				productId: String,
				name: String,
				price: Number,
				quantity: Number,
				subtotal: Number,
			},
		],
		orders: [
			{
				orderId: {
					type: String,
					required: true,
				},
			},
		],
		isAdmin: {
			type: Boolean,
			default: false,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("User", userSchema);
