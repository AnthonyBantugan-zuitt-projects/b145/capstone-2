const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, `Product name is required`],
			unique: true,
		},
		description: {
			type: String,
			required: [true, `Description is required`],
		},
		price: {
			type: Number,
			required: [true, `Price is required`],
		},
		quantity: {
			type: Number,
			default: 0,
		},
		isActive: {
			type: Boolean,
			default: true,
		},
	},
	{
		timestamps: true,
	}
);

module.exports = mongoose.model("Product", productSchema);
